#!/usr/bin/env bash
gunicorn vitaminink.wsgi:application -b 0.0.0.0:80 \
--worker-connections 1000 \
--timeout 300 \
--reload