from rest_framework.serializers import ModelSerializer
from api.models import Artist, Tattoo


class ArtistSerializer(ModelSerializer):
    class Meta:
        model = Artist
        fields = ('first_name', 'last_name', 'bio', 'date_joined', 'image')


class TattooSerializer(ModelSerializer):
    class Meta:
        model = Tattoo
        fields = '__all__'
