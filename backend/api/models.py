from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.
class Artist(AbstractUser):
    bio = models.TextField(max_length=1000, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=False)
    image = models.ImageField(upload_to='artists')

    def __str__(self):
        return self.username

    class Meta:
        ordering = ['date_joined']


class Tattoo(models.Model):
    artist = models.ForeignKey(Artist, related_name='tattoos',
                               on_delete=models.CASCADE)
    description = models.TextField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    image = models.ImageField(upload_to='tattoos')

    class Meta:
        ordering = ['-created_at']
