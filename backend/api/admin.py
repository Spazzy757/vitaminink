# users/admin.py
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import ArtistCreationForm, ArtistChangeForm
from .models import Artist, Tattoo


class ArtistAdmin(UserAdmin):
    add_form = ArtistCreationForm
    form = ArtistChangeForm
    model = Artist
    list_display = ['email', 'username']
    fieldsets = UserAdmin.fieldsets + (
      (None, {'fields': ('bio', 'location', 'birth_date', 'image')}),
    )


admin.site.register(Artist, ArtistAdmin)
admin.site.register(Tattoo)
