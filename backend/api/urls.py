from rest_framework.routers import DefaultRouter
from django.urls import re_path, include
from api.api import ArtistViewSet

router = DefaultRouter()
router.register(prefix='artist',
                viewset=ArtistViewSet,
                base_name='artists')

urlpatterns = [
    re_path(r'^', include(router.urls)),
]
