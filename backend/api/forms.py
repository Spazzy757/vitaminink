# users/forms.py
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from api.models import Artist


class ArtistCreationForm(UserCreationForm):

    class Meta:
        model = Artist
        fields = ('username', 'email', 'bio')


class ArtistChangeForm(UserChangeForm):

    class Meta:
        model = Artist
        fields = ('username', 'email', 'bio')
