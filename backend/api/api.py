from api.serializers import ArtistSerializer, TattooSerializer
from rest_framework.viewsets import ReadOnlyModelViewSet
from api.models import Artist, Tattoo


class ArtistViewSet(ReadOnlyModelViewSet):
    queryset = Artist.objects.all()
    serializer_class = ArtistSerializer


class TattooViewSet(ReadOnlyModelViewSet):
    queryset = Tattoo.objects.all()
    serializer_class = TattooSerializer
